import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.red[50],
        appBar: AppBar(
          backgroundColor: Colors.deepPurpleAccent,
          title: Text("List DO"),
          centerTitle: true,
        ),
        body: Column(
          children: [
            Text("Main Screen"),
            ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(Colors.deepPurpleAccent)),
                onPressed: () {
                  Navigator.pushNamed(context, '/todo');
                },
                child: Text("Go to next page"))
          ],
        ));
  }
}
